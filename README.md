# Hexo 在 [21云盒子](https://www.21cloudbox.com/)的示例

这是 [21云盒子](http://www.21cloudbox.com/) 上创建的 [Hexo](https://hexo.io/zh-cn/) 示例。

这个应用已经部署在 [https://hexo.21cloudbox.com](https://hexo.21cloudbox.com)。

## 部署

详情看 [https://www.21cloudbox.com/blog/solutions/how-to-deploy-hexo-static-site-in-production-server.html](https://www.21cloudbox.com/blog/solutions/how-to-deploy-hexo-static-site-in-production-server.html)

## 国内加速入口
* [Gitee](https://gitee.com/eryiyunbox-examples/hello-hexo)